input: float[] interpolated points
output: float[] normalized points
begin
  float min point = first point
        max point = first point
        max scale value = 1
        min scale value = -1

  float[] normalized

  while index < number of points
    if current point > max point
      max point = current point
  end

  while index < number of points
    if min point > current point
      min point = current point
  end

  point range = max point - min point
  scale range = max scale value - min scale value

  while i < number of points
    normalized points = ()(scale range * (current point - min point)) / point range) + min scale
  end
  
  return normalized points
end
