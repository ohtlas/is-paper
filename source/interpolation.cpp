input: Vector3 points
output: Vector3[] interpolated points
begin
  for j < number of points
    shift x values in points such that the minimum is at 0
  float scale is assigned to the largest x value multiplied by 1000 then divided by the number of points to be interpolated

  for i < number of to be interpolated
    for k < number of given points
      if the x values of the given point and desired point are equal
        interpolated points = given points
      else if x values are not equal
        percentage = ((x - x0)/(x1 - x0))
        interpolated point = (1 - percentage) * start + (percentage * finish)
        interpolated point is added to the end of the array of interpolated points
      update last point of desired points to equal the last given point
      return interpolated points
end
