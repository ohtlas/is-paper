input: normalized amplitude values
output: void
begin
  new List<float> temporary waveform
  for i < number of normalized points
    add normalized point i to the temporary waveform
    write temporary waveform at i to file
  new waveform = temporary waveform
  flush StreamWriter
end
