input: vector3 controller location
output: sound from audio buffer
begin
  if controller trigger is held
    vector3 position = controller location
    position is constrained to wall parallel to the z-axis
    the x value of position is compensated to supply accurate data to the interpolation method
  if controller trigger is released
    call calculate waveform method
      input: positions
      output: interpolated and normalized amplitude values
    call update waveform
      input: interpolated and normalized amplitude values
      output: sound from audio buffer
end
