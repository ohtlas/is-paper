input: vector3 controller location
output: void
begin
  new mesh
  vector3 vs is assigned the vertices of the mesh

  for i < two times the number of quads
    vs is resized

  vector2 uvs is assigned the UVs of the mesh_line
  if number of quads == 4
    map UVs for each vertex of the mesh
  else if number of vertices %8 == 0
    map the UVs accordingly
  else
    map the UVs accordingly

  assign front-facing quads using triangles
  assign back-facing quads using triangles
  recalculate the mesh bounds
  recalculate the mesh normals

  display quads
end
