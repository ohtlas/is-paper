input: amplitude data, channels
output: sound from audio buffer
begin
  if the new waveform is not null
    waveform = new waveform
    new waveform = null;
    index = 0

  if clear is set to true
    new_waveform = null;
    clear waveform
    add 0 to the empty waveform
    index = 0
    clear = false

  for i < number of amplitude values
    data at i = waveform at index
    if there are two channels (stereo)
      data at i + 1 = data at i
    index = (index + 1) % number of amplitude values in the waveform
end
