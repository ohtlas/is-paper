input: vector3 controller location
output: gameObject
begin
  if controller trigger is pressed
    new gameObject
    set line width
    set line material
  if controller trigger is held
    new mesh_line
    vector3 position = controller location
    position is constrained to wall parallel to the z-axis
    call add point method from MeshLineRenderer to start to build the line
      input: position
      output: void
  display line
end
