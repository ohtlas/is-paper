input: nearest given point less than desired point, nearest given point greater than desired, percentage between the two points
output: desired interpolated value
begin
  interpolated point = (1 - percentage) * start - (percentage * finish)
  return interpolated point
end
