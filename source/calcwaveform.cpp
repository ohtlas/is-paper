input: list<Vector3> position
output: void
begin
  new mesh
  vector3[] points vector is created
  While i < the number of position vectors
    points vector is assigned the values from positions
  
  vector3[] interpolated points = the output from the interpolation method on the points vectors
  call vertices to float method
    input: interpolated points points array
    output: array of amplitude values to be normalized

  call normalize points
    input: amplitudes to be normalized
    output: normalized amplitude values

  while i < the number of amplitude values
    add the normalized amplitude values to the new waveform list

end
