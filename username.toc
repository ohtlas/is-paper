\contentsline {chapter}{Abstract}{iii}{chapter*.1}
\contentsline {chapter}{Dedication}{iv}{chapter*.2}
\contentsline {chapter}{Acknowledgments}{v}{chapter*.3}
\contentsline {chapter}{Vita}{vi}{chapter*.4}
\contentsline {chapter}{Contents}{vii}{section*.5}
\contentsline {chapter}{List of Figures}{ix}{chapter*.7}
\contentsline {chapter}{List of Listings}{x}{chapter*.8}
\vskip .66\baselineskip \relax \hbox to \hsize {CHAPTER \hfil PAGE}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Defining Virtual Reality}{1}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Virtual Environment}{2}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Virtual Presence}{3}{subsection.1.1.2}
\contentsline {subsection}{\numberline {1.1.3}Sensory Feedback}{3}{subsection.1.1.3}
\contentsline {subsection}{\numberline {1.1.4}Interactivity}{4}{subsection.1.1.4}
\contentsline {section}{\numberline {1.2}Sound Manipulation in Context}{4}{section.1.2}
\contentsline {chapter}{\numberline {2}Audio Synthesis and Sampling}{6}{chapter.2}
\contentsline {section}{\numberline {2.1}Basics of Sound}{6}{section.2.1}
\contentsline {section}{\numberline {2.2}Waveforms and Their Characteristics}{9}{section.2.2}
\contentsline {section}{\numberline {2.3}Sampling and Filtering}{10}{section.2.3}
\contentsline {section}{\numberline {2.4}Synthesis}{11}{section.2.4}
\contentsline {chapter}{\numberline {3}Hardware, Software and Methods Used}{14}{chapter.3}
\contentsline {section}{\numberline {3.1}HTC Vive}{14}{section.3.1}
\contentsline {section}{\numberline {3.2}Unity Development Software}{15}{section.3.2}
\contentsline {section}{\numberline {3.3}Description of Assets Used}{16}{section.3.3}
\contentsline {section}{\numberline {3.4}Description of Code}{17}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Manager Script}{17}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Mesh Line Renderer Script}{19}{subsection.3.4.2}
\contentsline {subsubsection}{\numberline {3.4.2.1}Extend Line}{21}{subsubsection.3.4.2.1}
\contentsline {subsubsection}{\numberline {3.4.2.2}Calculate Waveform}{21}{subsubsection.3.4.2.2}
\contentsline {subsubsection}{\numberline {3.4.2.3}Interpolate Vectors}{22}{subsubsection.3.4.2.3}
\contentsline {subsubsection}{\numberline {3.4.2.4}Normalize Amplitudes}{24}{subsubsection.3.4.2.4}
\contentsline {subsection}{\numberline {3.4.3}Audio Managing Script}{25}{subsection.3.4.3}
\contentsline {section}{\numberline {3.5}Interacting with the Program}{28}{section.3.5}
\contentsline {chapter}{\numberline {4}Results and Analysis}{31}{chapter.4}
\contentsline {section}{\numberline {4.1}Survey of Test Subjects}{31}{section.4.1}
\contentsline {section}{\numberline {4.2}Data Collected from Participants}{33}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Warm vs. Bright}{35}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Smooth vs. Buzzy}{36}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Full vs. Hollow}{37}{subsection.4.2.3}
\contentsline {subsection}{\numberline {4.2.4}Custom Waveforms}{38}{subsection.4.2.4}
\contentsline {section}{\numberline {4.3}Data Analysis}{45}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Example Waveforms}{45}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Custom Waveforms}{47}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}Response to the Software}{49}{subsection.4.3.3}
\contentsline {chapter}{\numberline {5}Conclusion}{51}{chapter.5}
\contentsline {section}{\numberline {5.1}Future Works}{51}{section.5.1}
\contentsline {section}{\numberline {5.2}Concluding Thoughts}{51}{section.5.2}
\vskip .66\baselineskip \relax \hbox to \hsize {APPENDIX \hfil PAGE}
\contentsline {chapter}{\numberline {A}C Sharp Code}{53}{appendix.A}
\contentsline {chapter}{References}{66}{appendix*.38}
\contentsline {chapter}{Index}{67}{section*.39}
