%!TEX root = ../username.tex
\chapter{Introduction}\label{intro}

One of the key aspects of sound synthesis is the understanding of how wave shape affects the qualities of the tone. This paper focuses on showing users the this relationship. This chapter discusses basic background information on virtual reality (VR) and the origins of digital sound manipulation. A VR application is created to show users how timbre is affected by the shape of the wave. We ask users qualitative questions about their background knowledge and the sounds they hear to measure their understanding of waveforms before and throughout a software test. The goal of this study is to combine theory of sound synthesis and virtual reality into an application such that a user can learn from the experience.

\section{Defining Virtual Reality}

A general definition of VR is needed, as many individuals have their own impression of VR. For the purposes of this paper, we use this definition from Mihelj, Novak, and Begus: 
\begin{quote}
	"Virtual reality is composed of an interactive computer simulation, which senses the user's state and operation and replaces or augments sensory feedback information to one or more senses in a way that the user gets a sense of being immersed in the simulation (virtual environment). We can thus identify four basic elements of virtual reality: the virtual environment, virtual presence, sensory feedback (as a response to the user's actions) and interactivity"\cite{mihelj_introduction_2014}.
\end{quote}

In this study, we primarily discuss the sensory feedback as well as the interactivity of the software, but we also touch upon the virtual environment. We use VR as a medium to make drawing a waveform easier for the user in comparison to trying to draw a waveform with a mouse, in which there tends to be more restriction on movement.

\subsection{Virtual Environment}

To better understand how VR works, we must define the basic elements. The virtual environment can be described as the observable objects that are displayed by the software. The virtual environment is determined by the objects and characters within it, that are displayed through various senses, such as vision, touch, and sound. Objects in the virtual environment have properties, similar to that of objects in the real world, though these properties are up to the discretion of the developers. Some objects in virtual reality can have visual properties, but no haptic properties, causing the user to be able to see but not necessarily touch or physically interact with the object \cite{mihelj_introduction_2014}.

Sherman and Craig, in their book \textit{Understanding Virtual Reality}, define a virtual world to be \cite{sherman_understanding_2002}:

\begin{quote}
	1. an imaginary space often manifested through a medium.\\
	2. a description of a collection of objects in a space and the rules and relationships governing those objects.
\end{quote}

\subsection{Virtual Presence}

Virtual presence, or immersion, can be divided into two categories, physical and mental immersion. Though this study does not particularly focus on immersion in relation to the software test, it is still required for the creation of the software and to maintain the user's focus and minimize distractions during the software test. In the case of this study, Sherman and Craig's definition of the three key terms are used  \cite{sherman_understanding_2002}:

\begin{quote}
	\textbf{Immersion:} the sensation of being in an environment; can be a purely mental state or can be accomplished through physical means: physical immersion is a fefining characteristic of virtual reality; mental immersion is probably the goal of most media creators.\\
	\textbf{Mental immersion:} state of being deeply engaged; suspension of disbelief; involvement.\\
	\textbf{Physical immersion:} bodily entering into a medium; synthetic stimulus of the body's senses via the use of technology; this does not imply all senses or that the entire body is immersed/engulfed.
\end{quote}
 
 Our goal is to provide some level of physical immersion as well as mental immersion to allow the user to focus on the virtual world and ignore the real world while still knowing the difference between real and virtual worlds. Physical immersion in VR systems render the environment through vision, souHTCnd, and haptics. We aim to have the user be more physically immersed through the use of the  Vive as an interface to trigger certain stimuli in the user's senses.

\subsection{Sensory Feedback}

Sensory feedback is one of the most important elements for immersion within virtual reality. The VR software must provide sensory feedback on the user's physical location, typically through visual information, though some applications provide haptic information. The virtual environment must be in proportion to the user such that they are able to accurately move around the virtual environment. The software must also have the ability to automatically measure the location and orientation of real world objects in the physical environment, such that the user is not interrupted in the virtual world without being aware of the real world objects \cite{mihelj_introduction_2014}. In the case of this study, this visual information is provided by SteamVR's camera functionality though the HTC Vive's hardware.

\subsection{Interactivity}

To be considered realistic, VR software needs to respond to the user's actions. The software needs to give the user a sense of involvement through the interaction with objects, characters, and places within the computer-generated virtual environment, but this is only one form of interactivity. The ability to change one's location and view within a world is another form of interactivity, but virtual reality is more closely associated with the manipulation of objects physically within the virtual world. This study focuses primarily on the interactivity of the software, in which the user can draw waveforms and they receive an audible output of the waveform that they create. The software portion of this study does have a static world, meaning the objects in the environment are not able to move, but the user is able to move around the environment, draw on the wall and move a slider, providing only the required amount of interactivity \cite{sherman_understanding_2002}.


\section{Sound Manipulation in Context}

The ability to manipulate the characteristics of sound through the use of sound synthesis and sampling came with the origin of the analog synthesizers in the 1960s. These synthesizers were not computer based, but were completely reliant on controlling electrical voltage to create an analog of a sound wave. Soon after analog synthesizers became popular, samplers were created to mimic the sound of acoustic instruments through the use of a keyboard. They were able to do this by having sampled recordings of acoustic instruments saved on the device, and allowing each key on the keyboard to change the pitch of the recorded instrument \cite{ballora_essentials_2003}. With the rise in popularity of synthesizers and samplers in the 1980s and the improvement of computers, digital synthesizers were developed \cite{dodge_computer_1997}. The original digital synthesizers used a combination of waveform synthesis and waveform sampling, similar to the software that is used in this study. Presently, there are many complex digital synthesizers using various algorithms to create unique sounds.

In the following chapters, we further discuss the background of sound waves, looking into the physics as well as the processes of sound wave sampling and synthesis. Starting with the basic physics of sound waves, we show how their tonal qualities are affected by their physical characteristics. We then discuss how this information ties into the processes of sampling and synthesis, later showing how we utilize these concepts in the software portion of this study.
