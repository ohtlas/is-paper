%!TEX root = ../username.tex
\chapter{Results and Analysis}
\section{Survey of Test Subjects}
\begin{figure}
	\centering
	\includegraphics[totalheight=7cm]{figures/compareWave.pdf}
	\caption{Sine wave (a.), square wave (b.), and a sawtooth wave displayed in Audacity \cite{noauthor_audacity_nodate}.}
	\label{fig:compareWave}
\end{figure}
In this study, a small group of participants are first asked about their general knowledge of waveforms and tone qualities. They are asked to draw a sine wave, a square wave, a sawtooth wave, and a few waveforms of their choice. They are then asked to explain the tone quality, or timbre, of each waveform they draw and list the notable characteristics and critique the software.\\ \\

The importance of having the participants draw each type of wave not only displays their knowledge of the shape of each waveform, but also allows the participant to experience differences in timbre and learn about the basic wave shapes that are used in sound synthesis. The sine wave is used as the first example to show how the sound of one of the most basic periodic waveforms is used to form other distinct waveforms. The square wave and sawtooth wave are used to show how a difference in the shape of the wave can affect the qualitative aspects of the sound. A comparison of these three waveforms can be seen in Figure \ref{fig:compareWave}. The participant's custom drawing is not only used to reinforce the difference in timbre, but also for the participant to find a waveform pleasing to them, to experiment with the software and critique it.

When the participant is asked to explain the timbre of the waveforms, they are first asked a series of questions about each drawing and the resulting sound. Since there are no exact opposites in terms of timbre, some contrasting descriptors are chosen: 
\begin{quote}
	$\bullet$ Can you describe the sound of this waveform?\newline
	$\bullet$ Based on your previous knowledge of waveforms, what characteristic about the shape causes it to fit your previous description?\newline
	$\bullet$ Does this waveform sound warm, bright, or in between/can't tell the difference?\newline
	$\bullet$ Does this waveform sound buzzy, smooth, or in between/can't tell the difference?\newline
	$\bullet$ Does this wave sound full, hollow, or in between/can't tell the difference?\newline
\end{quote}
After each drawing of their choice they are asked:
\begin{quote}
	$\bullet$ Can you please explain the shape of the waveform drawn?\newline
	$\bullet$ Can you explain the timbre of this wave?\newline
	$\bullet$ Which of the previous examples is most similar in timbre to the one you have drawn?\newline
	$\bullet$ Does this waveform sound warm, bright, or in between/can't tell the difference?\newline
	$\bullet$ Does this waveform sound buzzy, smooth, or in between/can't tell the difference?\newline
	$\bullet$ Does this waveform sound full, hollow, or in between/can't tell the difference?\newline
\end{quote}

For the critiquing process, the participants are asked a few questions. Primarily, they are asked about the usability of the software and their experience. To expand upon the the question of usability, the participants are asked about anything they would change about the software, as well as listing an aspect that they learned from. Finally, they are asked if they learned anything new about waveforms and sound synthesis.

\section{Data Collected from Participants}

To maintain anonymity, participants are not recorded visually or audibly and not directly quoted. This is to ensure the privacy of the participants. A summary of each participant's answers to the questions is kept to gauge their knowledge of waveforms as well as drawings of the waveforms are saved to be analyzed for this study. All of the participants volunteered to test this software.

Of the 18 participants, only 4 have prior knowledge of the basic shapes of waveforms, and of the four, only two of participants have some knowledge of the affect shape has on timbre. The other 14 participants were asked to give their best guess to draw the waveforms asked, if they were unable to do so in an accurate manner, an example is provided to play the correct timbre.

When asked to describe the waveforms in their own words, participants mainly describe the sine wave as a "soft" sounding tone though some found it to be "metallic" or "tinny," whereas the square wave is described as "harsh" and "shrill." The majority of participants describe the sawtooth wave as "harsh," "intense," or somewhere in between the previous two examples. The majority think it sounds closer to the timbre of the square wave, but say it is more full sounding. An example of the user drawn waveforms can be seen in Figure \ref{fig:drawnExamples}.

\begin{figure}
	\begin{minipage}{.5\textwidth}
		\centering
		\includegraphics[totalheight=5cm]{figures/Examples/sine.png}
	\end{minipage}%
	\begin{minipage}{.5\textwidth}
		\centering
		\includegraphics[totalheight=5cm]{figures/Examples/square.png}
	\end{minipage}
	\begin{minipage}{.5\textwidth}
		\centering
		\includegraphics[totalheight=5cm]{figures/Examples/sawtooth.png}
	\end{minipage}
	\caption{User drawn examples of a sine wave (top left), a square wave (top right), and a sawtooth wave (bottom).}
	\label{fig:drawnExamples}
\end{figure}

\subsection{Warm vs. Bright}

\begin{table}[]
	\centering
	\begin{tabular}{l|l|l|l|}
		\cline{2-4}
		& Warm & Bright & Don't Know/In Between \\ \hline
		\multicolumn{1}{|l|}{Sine}     & 14   & 2      & 2          \\ \hline
		\multicolumn{1}{|l|}{Square}   & 0    & 15     & 3          \\ \hline
		\multicolumn{1}{|l|}{Sawtooth} & 0    & 16      & 2          \\ \hline
	\end{tabular}
	\caption{Warm vs. Bright}
	\label{tbl:WvB}
\end{table}

The participants were asked three questions in which they chose between two specific subjective descriptors of each waveform. The first terms they were asked to choose between were warm or bright as seen in Table \ref{tbl:WvB}. A warm timbre is generally observed when the waveform has gradual changes in slope of the amplitude with no or few sudden changes resulting in a evenly distributed harmonic energy. In contrast, a bright timbre is typically observed when there are some or many sudden changes in the slope of the amplitude, resulting in a jagged waveform with a lot of harmonic energy \cite{dodge_computer_1997}.

 When asked to describe the sine wave, 78\% of the participants chose to describe it as warm, with only 11\% describing it as bright and 11\% of participants stating that they are unable to tell. The majority of participants think the warm characteristic is due to the curves in the waves drawn, though one participant mentioned the lack of harmonic energy. The square wave is characterized as bright by 83\% and 17\% are unable to tell or believe it to be somewhere in between. In this case, the participants that considered it to be bright and have some knowledge of waveforms all stated that it is due to the waveform having drastic changes in slope, creating corners and vertical portions. The sawtooth wave is considered the most bright with only 11\% of participants stating that they did not know or say it was somewhere in between warm and bright, and 89\% chose bright. Those that classify the waveform as bright thinks it is due to the portion of the wave with a vertex or corner, causing it to change slope suddenly at a single point.

\subsection{Smooth vs. Buzzy}

\begin{table}[]
	\centering
	\begin{tabular}{l|l|l|l|}
		\cline{2-4}
		& Smooth & Buzzy & Don't Know/In Between \\ \hline
		\multicolumn{1}{|l|}{Sine}     & 15     & 3     & 0          \\ \hline
		\multicolumn{1}{|l|}{Square}   & 1      & 16    & 1          \\ \hline
		\multicolumn{1}{|l|}{Sawtooth} & 0      & 18    & 0          \\ \hline
	\end{tabular}
	\caption{Smooth vs. Buzzy}
	\label{tbl:SvB}
\end{table}

The participants were next asked to describe the sound waves as smooth or buzzy as seen in Table \ref{tbl:SvB}. Similar to that of a warm timbre, smooth sounding timbres are a result of a rounded waveform that has few or no partials, which can be epitomized by the sine wave. A buzzy timbre occurs when there are many partials present, causing extreme amounts high-harmonic energy \cite{dodge_computer_1997}. 

In the case of the sine wave, 83\% of participants chose to describe the waveform as smooth and 17\% of participants choosing buzzy. The participants had similar explanations as to why the sine wave is described as smooth rather than buzzy due to the rounded nature of a sine wave. One participant describes how a sine wave is just a pure tone with no harmonic elements. The participants that considered the timbre to be buzzy were unable to distinguish the physical characteristics that should cause this waveform to sound accordingly, mostly due to lack of previous knowledge. Almost every participant describes the square wave as buzzy, with one participant stating that it was smooth, and one participant unable to tell the difference. The reasoning the majority of the participants provided is due to the vertical portions of the wave causing large, instantaneous change in amplitude. The participant that considers the timbre to sound smooth states that it is due to the oscillation between amplitudes looking similar to that of a sine wave. In the case of the sawtooth wave, all participants considered the waveform to be  buzzy. Those that considered it buzzy state that it is the same or similar reasoning as to why the square wave is buzzy, that the waveform is described to have instantaneous jumps in amplitude. One participant states that the buzzy timbre is due to the sawtooth wave having many partials.

\subsection{Full vs. Hollow}

\begin{table}[]
	\centering
	\begin{tabular}{l|l|l|l|}
		\cline{2-4}
		& Full & Hollow & Don't Know/In Between \\ \hline
		\multicolumn{1}{|l|}{Sine}     & 8   & 7    & 3          \\ \hline
		\multicolumn{1}{|l|}{Square}   & 2    & 14   & 2          \\ \hline
		\multicolumn{1}{|l|}{Sawtooth} & 8    & 5   & 5          \\ \hline
	\end{tabular}
	\caption{Full vs. Hollow}
	\label{tbl:FvT}
\end{table}

The last comparison between descriptive words is between the waveform sounding full or hollow, seen in Table \ref{tbl:FvT}. A full timbre is characterized by having no gaps between integer harmonics, as seen in the sawtooth wave and the sine wave. In the case of the sine wave, there are no partials, therefore there are no gaps. A hollow timbre occurs when there are gaps in between integer harmonics, exemplified by the square wave, which only has odd harmonics. This question seemed to be the most difficult for the participants to decide upon. Of the three waveforms drawn, there are a total of ten occurrences in which a participant can not decide between the two options or think the timbre is somewhere in between the descriptors. 

The sine wave is stated to have a full sound by 44\% of the participants, 39\% of which think the wave sounds hollow and 17\% of participants did not know. Many of the participants that describe the waveform as full state that it is due to the curved nature of the waveform or that it is due to having no immediate changes in slope. One of the participants that describes the waveform as hollow states it is due to the lack of variance in amplitude in the peaks and troughs between each period of the drawn waveform. Most of the participants who describe it as hollow are unable to tell which characteristic of the waveform caused this. The square wave is characterized as hollow by 78\% of participants, with 11\% stating it sounds full and 11\% stating that they can not tell the difference or that it is in between. The majority of participants that state the waveform is hollow had similar explanations to those in the previous questions, stating the cause is the immediate, drastic change in amplitude. Some believe the reason that the timbre is hollow is from the lack of amplitude values in between the high and low values. Those that consider the waveform full believe it to be due to the oscillation between only the high and low values. The majority of participants classified the sawtooth wave as full at 44\%, with 28\% classifying the waveform as hollow, and 28\% say in between/did not know. The primary reason that the waveform is considered full by the participants is due to the shape being more similar to that of the sine wave than the square wave, but some mention the reason being the inclusion of all amplitude values between the minimum and maximum values.
 
 \subsection{Custom Waveforms}
 
 \begin{figure}
 	\begin{minipage}{.5\textwidth}
 		\centering
 		\includegraphics[totalheight=5cm]{figures/CustomSine/Round-Heartbeat.png}
 		\caption*{a.}
 	\end{minipage}%
 	\begin{minipage}{.5\textwidth}
 		\centering
 		\includegraphics[totalheight=5cm]{figures/CustomSine/Varying-Sine-1.png}
 		\caption*{b.}
 	\end{minipage}
 	\begin{minipage}{.5\textwidth}
 		\centering
 		\includegraphics[totalheight=5cm]{figures/CustomSine/nut.png}
 		\caption*{c.}
 	\end{minipage}
 \begin{minipage}{.5\textwidth}
 	\centering
 	\includegraphics[totalheight=5cm]{figures/CustomSine/Varying-Sine-2.png}
 	\caption*{d.}
 \end{minipage}
 \begin{minipage}{.5\textwidth}
 	\centering
 	\includegraphics[totalheight=5cm]{figures/CustomSine/Varying-Sine-3.png}
 	\caption*{e.}
 \end{minipage}
 \begin{minipage}{.5\textwidth}
 	\centering
 	\includegraphics[totalheight=5cm]{figures/CustomSine/Varying-Sine-4.png}
 	\caption*{f.}
 \end{minipage}
 	\label{fig:drawnExamplesAF}
 \end{figure}

 \begin{figure}
	\begin{minipage}{.5\textwidth}
		\centering
		\includegraphics[totalheight=5cm]{figures/CustomSine/Jaggy-Sine-1.png}
		\caption*{g.}
	\end{minipage}%
	\begin{minipage}{.5\textwidth}
		\centering
		\includegraphics[totalheight=5cm]{figures/CustomSine/Jaggy-Sine-2.png}
		\caption*{h.}
	\end{minipage}
	\begin{minipage}{.5\textwidth}
		\centering
		\includegraphics[totalheight=5cm]{figures/CustomSquare/Left-Round-Square.png}
		\caption*{i.}
	\end{minipage}
	\begin{minipage}{.5\textwidth}
		\centering
		\includegraphics[totalheight=5cm]{figures/CustomSquare/Right-Round-Square.png}
		\caption*{j.}
	\end{minipage}
	\begin{minipage}{.5\textwidth}
		\centering
		\includegraphics[totalheight=5cm]{figures/CustomSquare/Additive-Square.png}
		\caption*{k.}
	\end{minipage}
	\begin{minipage}{.5\textwidth}
		\centering
		\includegraphics[totalheight=5cm]{figures/CustomSawtooth/Varying-Sawtooth-1.png}
		\caption*{l.}
	\end{minipage}
	\label{fig:drawnExamplesGL}
\end{figure}

 \begin{figure}
	\begin{minipage}{.5\textwidth}
		\centering
		\includegraphics[totalheight=5cm]{figures/CustomSawtooth/Sine-Sawtooth.png}
		\caption*{m.}
	\end{minipage}%
	\begin{minipage}{.5\textwidth}
		\centering
		\includegraphics[totalheight=5cm]{figures/CustomSawtooth/Varying-Sawtooth-2.png}
		\caption*{n.}
	\end{minipage}
	\begin{minipage}{.5\textwidth}
		\centering
		\includegraphics[totalheight=5cm]{figures/CustomSawtooth/Varying-Sawtooth-3.png}
		\caption*{o.}
	\end{minipage}
	\begin{minipage}{.5\textwidth}
		\centering
		\includegraphics[totalheight=5cm]{figures/CustomSawtooth/Concave-Down-Sawtooth.png}
		\caption*{p.}
	\end{minipage}
	\begin{minipage}{.5\textwidth}
		\centering
		\includegraphics[totalheight=5cm]{figures/CustomSawtooth/Varying-Sawtooth-4.png}
		\caption*{q.}
	\end{minipage}
	\begin{minipage}{.5\textwidth}
		\centering
		\includegraphics[totalheight=5cm]{figures/CustomSawtooth/Concave-Up-Sawtooth.png}
		\caption*{r.}
	\end{minipage}
	\caption{User drawn custom waveforms.}
	\label{fig:drawnExamplesMR}
\end{figure}
 
 Participants are asked to draw a custom waveform, in any shape of their choosing. The majority chose to draw periodic waveforms, while others attempted to draw shapes and write words. The primary focus of having the participant draw a custom waveform is to have them analyze the timbre of their drawing in relation to the waveforms that they had previously reflected upon. Select custom waveforms are displayed and analyzed for the purpose of understanding the degree in which participants understand how certain characteristics affect timbre. The participants first had to describe visual characteristics of their waveform, many of which were most similar to that of the sine wave, but with varying amplitudes at each peak and trough.

Of the custom waveforms, a few stood out among the rest in shape and timbre. One participant chose to draw a waveform with the characteristics of a heartbeat with some rounded peaks and troughs as seen in Figure \ref{fig:drawnExamplesMR} (a.). The waveform drawn is mostly sinusoidal in nature, but the starting and ending amplitudes are at different values, resulting in a buzz in an otherwise warm and full sounding waveform. When asked to describe the timbre they relate it closest to the sine wave in timbre, stating that it is warm. They then classify it as warm and slightly buzzy, drawing upon the almost vertical nature of the shape for the reason for the buzz, and that it sounds full. The participant is unable to explain why they thought the waveform sounds full.

One participant attempted to draw a short word in cursive, shown in Figure \ref{fig:drawnExamplesMR} (c.). The resulting sample is only partially captured due to the nature of the software, as described in Chapter 3. Since the software can only sample functions with only one $y$ value per $x$ value, the resulting waveform appears to be mostly rounded due to the shape of the word, but with a few vertical and jagged portions towards the right end of the waveform. The participant states that the timbre of the waveform sounds closest to that of the sine wave example. The resulting timbre of the waveform is described as bright because there are few jagged portions of the waveform. The participant then classifies the timbre as buzzy due to the roughness of the waveform. The participant describes the timbre of this waveform as full, though the participant is unable to state why. Due to the nature of this waveform there are a few partials, leading the waveform to be bright, buzzy, and full in timbre.

The majority of other participants attempted to draw waveforms similar to the examples with minor differences to alter the timbre. One chose to draw a waveform similar to that of a sawtooth wave, altering the rise of the wave to have a concave-up slope, displayed in Figure \ref{fig:drawnExamplesMR} (r.). The participant states that the waveform is most similar in timbre to that of the sawtooth wave. They state that it is bright and buzzy due to the jaggedness of the shape, but warmer than the original sawtooth wave. Due to the shape of the waveform, there are many more harmonics than that of a sine wave. The participant states that the timbre sounds full due to the similarity in shape between this waveform and the sawtooth example.

Another drawing similar to that of one of the three example waves has a shape that appears to be a hybridization of a sawtooth wave and a sine wave, as in Figure \ref{fig:drawnExamplesMR} (m.). Each positive half cycle has a point, similar to that of a triangle wave, but each negative half cycle is rounded. The participant states that the timbre is brighter and more buzzy than that of a sine wave, but warmer than that of a sawtooth wave. They state that it sounds most similar to that of a sawtooth wave  but looks most similar to a sine wave, classifying the timbre of the waveform as warm and buzzy. The participant classifies the waveform as full.

Most of the remaining custom waves are adaptations of the examples, primarily augmentations of the sine wave. There were six participants of the remaining fourteen that drew adaptations of the sine wave, usually altering the maximum and minimum amplitude of certain periods of the wave and not others. This mainly resulted in waveforms that the participants classify as warm, smooth and full. Two of the six waveforms similar to that of the sine wave had some jagged edges on either the rise or fall of the period. This caused brighter tones that still sounded rather smooth and full to one of the participants, but the other considers the waveform smooth and hollow. 

Three participants made adaptations to the square wave. Two of the participants rounded one side of each half cycle of the square wave (one participant rounding the left, one rounding the right). The participant describing the tone quality of the waveform with a rounded left incline classifies the timbre as a warm, hollow tone with a slight buzz. The other states that the tone quality is warmer than the square wave, yet still bright, buzzy and hollow. The remaining participant that had drawn an adaptation of the square wave with a sinusoidal wave at each peak and trough described it as a slightly warmer version of the square wave.

The augmentations to the sawtooth wave by the remaining five participants are all either rounding the waveform to have a concave-down incline, or changing the maximum and minimum amplitude values for each cycle. These all result in timbres that are buzzy, but the waveform with concave-down incline has a hollow quality than the standard sawtooth wave. Four participants changed the maximum and minimum amplitude for each cycle and they noticed that the timbre is almost identical to that of the example sawtooth wave, stating that it is bright, buzzy and full, one stating that it sounds hollow.

Of the four participants that created waveforms that are rounded adaptations of the sine wave, almost all of the classifications are considered warm, smooth and full. Only one participant classifies their waveform as warm, buzzy and full. The two participants that created waveforms that have jagged inclines or declines and are adaptations of the sine wave classify their waveforms as bright and full, but one states that it is smooth and one states that it is buzzy. The three adaptations of the square wave all classify their waveforms as buzzy and hollow, with each stating that the waveform is warmer than the original square wave. The five participants that created waveforms that are adaptations of the sawtooth wave mostly state that their waveform is buzzy, but they vary in the other two categories. The three participants that rounded the sawtooth wave to have concave up declines state that the waveform's timbre is warm and buzzy, two of them state it is full, one stating that it is hollow. The participant that added concave down inclines to the sawtooth wave states that the waveform is bright, buzzy and hollow, similar to that of the square wave. The participant that varies the amplitude in each cycle of their waveform notices that the timbre is almost the same as the example sawtooth wave, classifying it as bright, buzzy and full.

\section{Data Analysis}

\subsection{Example Waveforms}
The data collected for this portion of the study is used to see how the participants were able to effectively use the software, and to gauge their knowledge of sound waves before and after the software test. The majority of participants expressed that they did not have any prior knowledge of the relationship between waveforms and timbre. Because of this, the classification of timbre for the example waveforms is less important than the understanding and classification of the timbre from their custom waveforms. None of the participants had prior knowledge of harmonics before completing this test, and this information was not discussed with the participants during the test. They are tasked with finding the characteristics of the shape that affect the number of harmonics present in a waveform. By the end of this test, the goal is to see how many individuals are able to correctly classify their custom waveform using the example descriptors as well as state the reason the characteristic of the shape that correlates to each descriptor. They should be able to notice:
\begin{quote}
	$\bullet$ rounded waveforms produce a warm timbre,\\
	$\bullet$ jagged waveforms produce a bright timbre,\\
	$\bullet$ waveforms with gradual slopes and no vertex produce a smooth timbre,\\
	$\bullet$ waveforms with a gradual or no slope, and sudden large jumps in amplitude values produce a buzzy timbre,\\
	$\bullet$ waveforms that have less area produce a full timbre,\\
	$\bullet$ waveforms that have a greater area under the curve produce a hollow timbre.\\
\end{quote} 

The sine wave portion of the test is primarily used to provide context and to contrast the other two examples as well as the custom waveforms drawn by the participant. This portion is also used to set an example of the most basic waveform. Sine waves are considered to have a warm, smooth and full quality. Note that the majority of participants classify the timbre similar to that of our classification of the sine wave for the first two descriptors. The participants that label the timbre with the same descriptor as ours are also mostly able to describe or give their best guess as to the reason the shape of the sine wave causes each qualitative aspect of the timbre.  They state that the reason is due to the round, smooth nature of the shape. The minority are mostly able to explain why they thought the shape of each wave causes the descriptor they chose. Some of the participant's drawings of the sine wave began and ended at different amplitudes, causing the timbre to not be quite accurate to a true sine wave. The classification of full or hollow has the most disparity. Some participants are unable to explain their reasoning for their classification due to lack of previous knowledge and lack of context.

To contrast the timbre of the sine wave, the square wave is drawn. The square wave is characterized as having a bright, buzzy, and hollow timbre. The contrast between these waves is shown to provide the participants with more context to better grasp the meaning behind the descriptors and the physical characteristics that cause them. The majority of participants classify this timbre as bright, buzzy, and hollow, matching our description. When put in context along with the sine wave, most participants are able to better understand the descriptors. In the case of the square wave, 85\% of the classifications are the same as our outcome, whereas only 69\% of the classifications of the sine wave are the same as our outcome.

The sawtooth wave is the third example, used as a challenge to see if participants can classify the timbre of a waveform that has shared characteristics with the previous two examples. The sawtooth wave is generally classified as bright and buzzy, similar to the square wave, but full, similar to the sine wave. Due to the sawtooth wave having all even and odd harmonics, the waveform has a large amount of harmonic energy, causing it to sound bright and buzzy. The participants have similar classifications to ours in the first two categories, but there was a disparity in the final question about whether it was full or hollow. Since the timbre is more full than the square wave, but more hollow than the sine wave, the classification of the wave as full vs. hollow can be considered as in between. Typically, though, the timbre of a sawtooth wave is considered full, and hence the variance in classification. Since this example is used as a challenge to prepare the participants for the classification of their custom drawing, the expectation of participants is not as high as with the classification of the square wave, and the results reflect this with 78\% of participants classifying this wave as bright, buzzy and full, matching our description.

\subsection{Custom Waveforms}

%\begin{table}[]
%	\centering
%	\caption{Participant timbre classification of custom waveforms (a.), and our timbre classification of custom waveforms (b.).}
%	\label{tbl:PCW}
%	\begin{tabular}{l|l|l|l|l|l|l|}
%		\cline{2-7}
%		a. & Warm & Bright & Smooth & Buzzy & Full & Hollow \\ \hline
%		\multicolumn{1}{|l|}{Sine}     & 6   & 2    & 5   & 3 & 7 & 1 \\ \hline
%		\multicolumn{1}{|l|}{Square}   & 1    & 2   & 3 & 0 & 0 & 3         \\ \hline
%		\multicolumn{1}{|l|}{Sawtooth} & 2    & 5   & 0 & 7 & 5 & 2        \\ \hline
%	\end{tabular}
%
%	\begin{tabular}{l|l|l|l|l|l|l|}
%		\cline{2-7}
%		b. & Warm & Bright & Smooth & Buzzy & Full & Hollow \\ \hline
%		\multicolumn{1}{|l|}{Sine}     & 5   & 3    & 5   & 3 & 8 & 0 \\ \hline
%		\multicolumn{1}{|l|}{Square}   & 0    & 3   & 3 & 0 & 0 & 3         \\ \hline
%		\multicolumn{1}{|l|}{Sawtooth} & 1    & 6   & 0 & 7 & 6 & 1        \\ \hline
%	\end{tabular}
%\end{table}

\begin{table}[]
	\centering
	\begin{tabular}{l|l|l|l|l|}
		\cline{2-5}
		& Closest Example Wave Shape & Warm / Bright & Smooth / Buzzy & Full / Hollow \\ \hline
		\multicolumn{1}{|l|}{User 1}  & Sine                           & Warm          & Buzzy          & Full          \\ \hline
		\multicolumn{1}{|l|}{User 2}  & Sine                           & Warm          & Smooth         & Full          \\ \hline
		\multicolumn{1}{|l|}{User 3}  & Sine                           & Bright        & Buzzy          & Full          \\ \hline
		\multicolumn{1}{|l|}{User 4}  & Sine                           & Warm          & Smooth         & Full          \\ \hline
		\multicolumn{1}{|l|}{User 5}  & Sine                           & Warm          & Smooth         & Full          \\ \hline
		\multicolumn{1}{|l|}{User 6}  & Sine                           & Warm          & Buzzy          & Full          \\ \hline
		\multicolumn{1}{|l|}{User 7}  & Sine                           & Bright        & Smooth         & Full          \\ \hline
		\multicolumn{1}{|l|}{User 8}  & Sine                           & Warm          & Smooth         & Hollow        \\ \hline
		\multicolumn{1}{|l|}{User 9}  & Square                         & Warm          & Buzzy          & Hollow        \\ \hline
		\multicolumn{1}{|l|}{User 10} & Square                         & Warm          & Buzzy          & Hollow        \\ \hline
		\multicolumn{1}{|l|}{User 11} & Square                         & Bright        & Buzzy          & Hollow        \\ \hline
		\multicolumn{1}{|l|}{User 12} & Sawtooth                       & Bright        & Buzzy          & Full          \\ \hline
		\multicolumn{1}{|l|}{User 13} & Sawtooth                       & Warm          & Buzzy          & Full          \\ \hline
		\multicolumn{1}{|l|}{User 14} & Sawtooth                       & Bright        & Buzzy          & Full          \\ \hline
		\multicolumn{1}{|l|}{User 15} & Sawtooth                       & Bright        & Buzzy          & Full          \\ \hline
		\multicolumn{1}{|l|}{User 16} & Sawtooth                       & Warm          & Buzzy          & Hollow        \\ \hline
		\multicolumn{1}{|l|}{User 17} & Sawtooth                       & Bright        & Buzzy          & Hollow        \\ \hline
		\multicolumn{1}{|l|}{User 18} & Sawtooth                       & Bright        & Buzzy          & Full          \\ \hline
	\end{tabular}
	\caption{Participant timbre classification of custom waveforms}
	\label{tbl:PCW}
\end{table}

\begin{table}[]
	\centering
	\begin{tabular}{l|l|l|l|l|}
		\cline{2-5}
		& Closest Example Wave Shape & Warm / Bright & Smooth / Buzzy & Full / Hollow \\ \hline
		\multicolumn{1}{|l|}{User 1}  & Sine                           & Warm          & Buzzy          & Full          \\ \hline
		\multicolumn{1}{|l|}{User 2}  & Sine                           & Warm          & Smooth         & Full          \\ \hline
		\multicolumn{1}{|l|}{User 3}  & Sine                           & Bright        & Buzzy          & Full          \\ \hline
		\multicolumn{1}{|l|}{User 4}  & Sine                           & Warm          & Smooth         & Full          \\ \hline
		\multicolumn{1}{|l|}{User 5}  & Sine                           & Warm          & Smooth         & Full          \\ \hline
		\multicolumn{1}{|l|}{User 6}  & Sine                           & Warm          & Buzzy          & Full          \\ \hline
		\multicolumn{1}{|l|}{User 7}  & Sine                           & Bright        & Smooth         & Full          \\ \hline
		\multicolumn{1}{|l|}{User 8}  & Sine                           & Bright        & Smooth         & Full          \\ \hline
		\multicolumn{1}{|l|}{User 9}  & Square                         & Warm          & Buzzy          & Hollow        \\ \hline
		\multicolumn{1}{|l|}{User 10} & Square                         & Warm          & Buzzy          & Hollow        \\ \hline
		\multicolumn{1}{|l|}{User 11} & Square                         & Bright        & Buzzy          & Hollow        \\ \hline
		\multicolumn{1}{|l|}{User 12} & Sawtooth                       & Bright        & Buzzy          & Full          \\ \hline
		\multicolumn{1}{|l|}{User 13} & Sawtooth                       & Warm          & Buzzy          & Full          \\ \hline
		\multicolumn{1}{|l|}{User 14} & Sawtooth                       & Bright        & Buzzy          & Full          \\ \hline
		\multicolumn{1}{|l|}{User 15} & Sawtooth                       & Bright        & Buzzy          & Full          \\ \hline
		\multicolumn{1}{|l|}{User 16} & Sawtooth                       & Bright        & Buzzy          & Hollow        \\ \hline
		\multicolumn{1}{|l|}{User 17} & Sawtooth                       & Bright        & Buzzy          & Full          \\ \hline
		\multicolumn{1}{|l|}{User 18} & Sawtooth                       & Bright        & Buzzy          & Full          \\ \hline
	\end{tabular}
	\caption{Our timbre classification of participant drawn custom waveforms}
	\label{tbl:OCW}
\end{table}

The custom waveform portion of this study is used to measure if there is growth in knowledge of waveforms among participants. When asked to compare their custom waveform with one of the examples, eight participants state that their waveform is most similar in shape to a sine wave, three state that their waveform is most similar to a square wave, and seven relate their wave to a sawtooth wave. These classifications are all the same as ours upon analyzing the shapes of the sampled figures.

In the classification of timbre for their waveforms, seen in Table \ref{tbl:PCW}, the participants that drew sine wave adaptations are close in classification to what we state. Based upon the shape, six of the waveforms have a warm timbre and are all classified by the participants accordingly, and two are bright. One of the waveforms that has a bright timbre is classified as warm by the user, despite the jagged nature of the waveform. According to our judgment on the shape, six of the waveforms are smooth and three are buzzy, and all are classified accordingly by the participants. All nine are full sounding, though one participant considers their waveform to be hollow sounding. This results in 92\% of the classifications being the same as ours for the sine wave.

The three waveforms related to the square wave are all bright, buzzy, and hollow. Only one participant classifies their waveform as warm rather than bright. This results in  89\% of classifications of the custom waveforms being the same as our result in this category.

We consider the six of the seven custom waveforms that the participants label most similar to the sawtooth wave to be buzzy and most participants classify that aspect in the same manner. We state that one of the waveforms are considered to be warm, and six bright, and these aspects are also identified by the participants. One of the six waveforms is hollow in timbre due to the large area under the curve, and six are considered full due to the smaller area under the curve. One participant classifies their waveform as hollow, despite the small area under the curve, resulting in 90\% of classifications being the same as our classification.

Overall, there is an increase in percentage of shared classifications between the participants and our analysis in the custom waveform portion of the test in comparison to the example waveform portion of the test, with an average of 77\% on the example portion and an average of 90\% on the custom waveform portion.

\subsection{Response to the Software}

Upon the completion of the software trial, participants were then asked about the usability. Most of the participants agreed that the software is usable, but some made suggestions to better improve the experience. Throughout the testing process, updates to the code were made to improve the user experience. One of the first criticisms that a participant bought forward is in reference to the pitch of the sound being played back. The participant states that the pitch was too high to hear clear tone qualities. In response, the default pitch of $440hz$ has been changed to $220hz$. Another change in the software that resulted from the user criticism is in the thickness of the line being drawn. A few participants state that the thickness of the line being used to draw the waveform was too hollow, and so a change to a thicker line is made to allow users to better see their drawings. The last large criticism is about the environment. The white wall in which the drawing appears has had a light grid added to it. This change is made to allow the participants to see the starting and ending location of their drawing, so they are able to better match the height of the amplitude to avoid an unintentional buzzy timbre to the sound.

When asked if there is a specific portion of the software that they learned from, the majority of participants responded that they learned more about timbre through the comparison of the example waveforms. Other participants state that they learned about tone qualities through the experimentation with custom waveforms and augmentation of aspects from the example sound waves to hear how the timbres differ. Every participant states that they learned at least some new information about waveforms and their characteristics by completing this software test. The majority of participants express that they learned how certain physical characteristics affect timbre, while some participants state that they learned more about how to analyze and describe a tone's timbre through the use of qualitative descriptors.