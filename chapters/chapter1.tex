%!TEX root = ../username.tex
\chapter{Audio Synthesis and Sampling}\label{text}
\section{Basics of Sound}\label{sec:newsec}
Sound is described to be rapid fluctuations in pressure with frequencies in the audible range of the human ear. Sound waves are formed by the compression and decompression of molecules based on these pressure fluctuations, and disruption of these fluctuations cause changes in the pressure and density that fluctuate around the equilibrium state \cite{mihelj_introduction_2014}. Compression is represented on the positive end of the equilibrium state (the density and pressure are increased), whereas during rarefaction, the density and pressure are decreased, represented on the negative end of the equilibrium state as seen in Figure \ref{fig:waveform_description}. The energy of a sound at a single point propagates out in a sphere, moving away from the source. When energy is transported through a medium, the medium tends to oscillate, and the energy is transported through the medium's oscillations. These oscillations are called waves. Though energy is transported through the oscillations, the molecules of the medium only move about their original position, thus allowing the medium to not be transported as well \cite{ballora_essentials_2003}.

\begin{figure}
	\centering
	\includegraphics[totalheight=5cm]{figures/waveform_description.pdf}
		\caption{A single period of a sine wave, showing compression and rarefaction.}
		\label{fig:waveform_description}
\end{figure}

Sound waves are periodic in nature and the amplitude, shape and wavelength of the period all determine how the wave is perceived by the ear. These waves are also longitudinal in nature, in which they oscillate in the same axis as their propagation \cite{ballora_essentials_2003}. The volume level, measured in decibels (dB), is determined by the amplitude of the wave, the frequency is determined by the wavelength, and the timbre, or tone quality, is determined by the shape of the wave.

This study mainly discusses the timbre of sound waves. Timbre is the distinct tone quality of a sound, which is much more difficult to characterize than volume or frequency, because unlike volume and frequency, it is multidimensional. Differences in timbre can be heard between different instruments, such as the sound of a guitar, piano, and trumpet. All of these instruments can play the same fundamental note, but they all have a slightly different quality to the pitch. A waveform of a guitar and piano in comparison to a sine wave can be visualized in Figure \ref{fig:wave_comparison}. Dodge and Jerse state that timbre is difficult to quantify because there exists no simple pair of opposites between which a scale can be made \cite{dodge_computer_1997}, though Ballora states that timbre is based on frequency content \cite{ballora_essentials_2003}. Certain qualitative descriptors used to describe tone quality (such as warm, bright, smooth, buzzy, etc.) are linked to certain patterns in frequency content. Bright tone qualities have high amounts of harmonic energy, which is linked to the number of harmonics a waveform contains. Harmonics are integer multiples of the fundamental frequency. Buzzy tone qualities have extreme amounts of high-harmonic energy, such as in a sawtooth wave \cite{dodge_computer_1997}.

\begin{figure}
	\centering
	\includegraphics[totalheight=4cm]{figures/wave_comparison.pdf}
		\caption{Comparison between a sine wave, a waveform of a guitar, and piano \cite{nov_explaining_nodate}.}
		\label{fig:wave_comparison}
\end{figure}

Tones tend to be characterized as consisting of a waveform enclosed in an amplitude envelope made up of three parts: the attack, steady state and the decay as seen in Figure \ref{fig:envelope}. The attack refers to the rise of the wave, and decay refers to the fall. The steady state is the state in between where the amplitude tends to stay relatively constant.

\begin{figure}
	\centering
	\includegraphics[totalheight=5cm]{figures/envelope.pdf}
		\caption{Elements of a sound envelope \cite{noauthor_sound_nodate}.}
		\label{fig:envelope}
\end{figure}

Changing some of the characteristics mentioned above can have very noticeable effects on the resulting sound. Elongating the attack of the sound wave causes it to reach its steady state at a later point in time, thus having the waveform sound like it's building up to the steady state. Shortening it results in a more percussive start to the sound. A short decay causes the fall of the wave to be almost immediate and sound like a cut in the audio, whereas a long decay does the opposite and elongates the release of the wave.

%
\section{Waveforms and Their Characteristics}
According to Dodge and Jerse, "the definition of the waveform versus time can be made by specifying the mathematical equation that relates the amplitude of the desired waveform to its phase" \cite{dodge_computer_1997}. This equation is typically a Fourier transform of sine waves, resulting in a waveform with a unique shape. A Fourier transform is a function in which any periodic signal may be expressed as the sum of a series of sinusoidal waves \cite{ballora_essentials_2003}. The phase of the waveform has minimal effect on the timbre of the sound, whereas the other aspects, such as frequency, amplitude, along with spectral components have a large impact on the timbre. Spectral components are also known as partials of the wave form and are used to describe the overtones of the sound \cite{dodge_computer_1997}. Partials are not necessarily harmonics. For example, bell sounds typically contain many inharmonic partials that are determined by the size and makeup of the bell \cite{ballora_essentials_2003}.

The spectrum of a waveform can be calculated by taking the Fourier transform of the waveform. The cycle of the waveform can be built by adding the sound-pressure patterns of the individual components of the spectrum, as seen in Figure \ref{fig:addingsinewaves}. This process is called additive synthesis, and is one of the most basic forms of synthesis.

\begin{figure}
	\centering
	\includegraphics[totalheight=7.5cm]{figures/addingsinewaves.pdf}
		\caption{The addition of two sine waves to produce a third waveform \cite{sievers_young_nodate}.}
		\label{fig:addingsinewaves}
\end{figure}

Similar to waves of water in a pool coming in contact with the pool wall or swimmers, sound waves cannot travel forever without coming into contact with an obstacle. If the object's face is smaller than that of the sound wave, then the wave refracts around the object and continues propagating in a new direction. Conversely, if an object is larger than the wave, then the wave is reflected and propagates in the same angle with which it struck the object \cite{ballora_essentials_2003}.

\section{Sampling and Filtering}

Audio sampling is the process of taking "samples" from a sound wave and pushing them to the audio buffer. The sampling rate describes how often a wave form is sampled per second. The sample is the value of the amplitude at a desired point along the wave. This provides a single number that is put into a list with the other sampled values to form a discrete approximation of a continuous waveform, as seen in Figure \ref{fig:samples_figure}. If the sample rate is too small, the resulting audio does not sound similar to that of the sound wave. According to Ballora, most audible frequencies are oversampled, meaning that the audio frequency is below the Nyquist frequency. Given a wave which is eventually to be smoothed by a subsequent filtering process, Nyquist created this theorem:  \newline

\begin{quote}
	To represent a signal containing frequency components up to $X$ Hz, it is necessary to use a sampling rate of at least $2X$ Hz.
\end{quote}
Conversely,

\begin{quote}
	The maximum frequency contained in a signal sampled at a rate of $SR$ Hz is calculated as $SR/2$ Hz \cite{ballora_essentials_2003}.
\end{quote}

\begin{figure}
	\centering
	\includegraphics[totalheight=2cm]{figures/samples_figure.pdf}
	\caption{a. A sinusoidal waveform in Audacity \cite{noauthor_audacity_nodate}. b. Sampling of the waveform from a.}
	\label{fig:samples_figure}
\end{figure}

The Nyquist frequency is said to be the critical sample rate. Sampling at the Nyquist frequency also runs the risk of missing the peaks and troughs of the sound wave and only sampling the zero crossings. In order for the critical sample rate to work, filters are used to removed all frequencies above the Nyquist frequency. The frequencies above the Nyquist frequency are considered undersampled and aliases to a frequency of $-(SR - F)$ where $F$ is a frequency sampled at a sampling rate of $SR$. Aliases are the misrepresented frequencies due to recording a frequency greater than the Nyquist frequency. The process of removing these frequencies is important to do at this stage because these aliases cannot be taken out later.

Filtering is the process of allowing some components of the sound wave to pass, while eliminating others. In this case, filtering is used in spectral shaping of the sampled audio in order to emphasize or deemphasize certain aspects of the waveform. There are four main filter types, and these include a Lowpass filter, Highpass filter, Bandpass filter, and a Band-reject filter. A Lowpass filter reduces or eliminates higher frequencies. This filter is used on the audio signal for the elimination of frequencies above the Nyquist frequency.

\begin{figure}
	\centering
	\includegraphics[totalheight=7cm]{figures/lowpass.pdf}
	\caption{Input (a.) and output of a low-pass filter (b.) in Audacity\cite{noauthor_audacity_nodate}.}
	\label{fig:Filter}
\end{figure}

\section{Synthesis}
Sound synthesis is "the creation of complex sounds from raw materials," \cite{ballora_essentials_2003}. In the 1950s, Max Mathews created the first computer audio synthesis programs at Bell Labs, and these programs led to the first exploration and techniques used for many of the principles of synthesis \cite{dodge_computer_1997}. In the 1980s, commercial digital synthesizers were introduced, where the synthesis algorithms were encoded into the operation systems of these devices \cite{ballora_essentials_2003}. 

Bell also came up with the concept of wavetables and modular construction. Wavetables are a collection of sample values representing one period of a waveform. The wavetables allow for interpolation. Interpolation becomes important when searching for a set of values in a wavetable that is not specifically listed in a wavetable. Based on the size of the wavetable and the sampling frequency, the resulting frequency, f, is calculated as \cite{ballora_essentials_2003}:

\begin{equation}
f = \frac{\text{Sampling Frequency}}{\text{Wavetable Size}}.
\end{equation}
To change the octave of the frequency, another variable must be introduced, the sampling increment. This gives us the equation

\begin{equation}
f = \frac{\text{Sampling increment} \times \text{Sampling Rate}}{\text{Wavetable Size}}.
\end{equation}
The sampling increment, $SI$, can be calculated by rewriting the previous equation as

\begin{equation}
SI = \text{Wavetable Size} \times \frac{\text{Desired Frequency}}{\text{Sampling Rate}}.
\end{equation}

The basic building blocks of software synthesis that are used to create complex instruments are termed unit generators. These are algorithms that create or modify audio signals. These unit generators take the amplitude envelope and combine it with the frequency to create the output as seen in Figure \ref{fig:sinewave_oscillator}. The same result can be achieved by applying a constant amplitude to the waveform. After, the signal is passed through the envelope generator to give the waveform the desired pattern of amplitude variation \cite{dodge_computer_1997}.

\begin{figure}
	\centering
	\includegraphics[totalheight=7.5cm]{figures/sinewave_oscillator.pdf}
	\caption{A simple sine wave oscillator using an envelope generator as an amplitude input.}
	\label{fig:sinewave_oscillator}
\end{figure}


